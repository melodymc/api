<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Currencies extends Migration
{
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->integer('exchange_currency_id')->unsigned()->nullable();
            $table->foreign('exchange_currency_id')->references('id')->on('currencies');
            $table->enum('changeType', [0, 1, 2, 3])->default(0); // 0 - запрет продажи, покупки, 1 можно купить и продать за указанную валюту по курсу, 2 можно только купить за указанную валюту, 3 можно только продать за указанную валюту
            $table->float('saleExchangeRate')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currencies');
    }
}
