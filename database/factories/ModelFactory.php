<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Pets\Account::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'uuid' => $faker->uuid
    ];
});

$factory->define(\App\Pets\Wallet::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(\App\Pets\Operation::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->boolean(),
        'value' => $faker->numberBetween(1, 10)
    ];
});

$factory->define(\App\Pets\Currency\Currency::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(\App\Pets\Pet::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->numberBetween(1, 10),
        'cost' => $faker->randomFloat()
    ];
});