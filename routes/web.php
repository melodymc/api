<?php

use App\Entities\Abstractions\Api\BasicApi;
use App\Utils\ClassUtils;

$router->get('/v{v}/allows', function () {
    return collect(ClassUtils::getEntityClasses())->filter(function ($class) {
        return is_subclass_of($class, BasicApi::class);
    })->map(function ($class) {
        return ClassUtils::getTypeByClass($class);
    })->values()->toArray();
});

$router->get('/v{v}/{type}[/{id}]', ['as' => '_get', 'uses' => 'ApiController@get']);
//$router->head('/v{v}/{type}[/{id}]', ['as' => '_head', 'uses' => 'ApiController@head']);
$router->post('/v{v}/{type}', ['as' => '_post', 'uses' => 'ApiController@post']);
$router->patch('/v{v}/{type}/{id}', ['as' => '_patch', 'uses' => 'ApiController@patch']);
$router->put('/v{v}/{type}/{id}', ['as' => '_put', 'uses' => 'ApiController@put']);
$router->delete('/v{v}/{type}/{id}', ['as' => '_delete', 'uses' => 'ApiController@delete']);
$router->options('/v{v}/{type}', ['as' => '_options', 'uses' => 'ApiController@options']);


$router->get('/ping', function () use ($router) {
    return 'pong';
});

$router->get('/test', function () use ($router) {
    return \App\Entities\Account::find(1)->getWallet(2)->getBalance();
});