<?php

namespace App\Http\Controllers;

use App\Entities\Abstractions\Api\DeleteTrait;
use App\Entities\Abstractions\Api\GetTrait;
use App\Entities\Abstractions\Api\PostTrait;
use App\Entities\Abstractions\Api\PatchTrait;
use App\Entities\Abstractions\Api\PutTrait;
use App\Entities\Economic\Currency\Currency;
use App\Utils\ClassUtils;
use App\Utils\ResponseUtils;
use App\Utils\TraitUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiController extends Controller
{

    /**
     * @param Request $request
     * @param int $v
     * @param $type
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    final public function get(Request $request, int $v, $type, $id = null) {
        $this->validate($request, ['data' => ['array'], 'type' => 'boolean']);
        $type = Str::singular($type);
        $mode = $request->get('mode');
        $data = $request->get('data');
        if($mode !== null) {
            $mode = (bool) $mode;
        }
        return self::postValidate($type, $v, function ($class, $v) use ($data, $mode, $request, $type, $id) {
            if($id !== null) {
                if($type === 'currency' && $id == 0) {
                    return Currency::getDefaultCurrency();
                }
                return $class::getCurrent($v, $id, $data, $mode);
            } else {
                return $class::getAll($v, $data, $mode);
            }
        }, GetTrait::class);
    }

    /**
     * @param Request $request
     * @param int $v
     * @param $type
     * @param int $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    final public function patch(Request $request, int $v, $type, int $id) {
        return self::postValidate($type, $v, function ($class, $v) use ($request, $id) {
            if($obj = $class::find($id)) {
                return $obj->_patch($v, $request->all());
            }
            return response()->make('', 404);
        }, PatchTrait::class);
    }

    /**
     * @param Request $request
     * @param int $v
     * @param $type
     * @param int $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    final public function put(Request $request, int $v, $type, int $id) {
        return self::postValidate($type, $v, function ($class, $v) use ($request, $id) {
            if($obj = $class::find($id)) {
                if($obj->_put($v, $request->all())) {
                    return response()->make('', 204);
                }
                return response()->make('some error', 500);
            }
            return response()->make('', 404);
        }, PutTrait::class);
    }

    /**
     * @param Request $request
     * @param int $v
     * @param $type
     * @param int $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    final public function delete(Request $request, int $v, $type, int $id) {
        return self::postValidate($type, $v, function ($class, $v) use ($type, $request, $id) {
            if($obj = $class::find($id)) {
                $obj->_delete($v);
                return ResponseUtils::returnRequest('', 204);
            }
            return ResponseUtils::returnRequest('', 404);
        }, DeleteTrait::class);
    }

    /**
     * Новое произвольное энтити, с проверкой доступности создания оного
     * @param Request $request
     * @param int $v
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    final public function post(Request $request, int $v, $type) {
        return self::postValidate($type, $v, function ($class, $v) use ($type, $request) {
            try{
                return response()->make('', 201, ["Location" => route('_get', ['v' => $v, 'type' => $type, 'id' => $class::_post($v, $request->all())['id']])]);
            } catch (\Exception $e) {
                return ResponseUtils::returnRequest($e->getMessage(), 500);
            }
        }, PostTrait::class);
    }

    /**
     * @param int $v
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    final public function options(int $v, $type) {
        return self::postValidate($type, $v, function ($class, $v) {
            return response()->json($class::_options($v), 200);
        });
    }

    /**
     * @param string $type
     * @param string $v
     * @param \Closure $callback
     * @param $trait
     * @return \Illuminate\Http\JsonResponse
     */
    final public static function postValidate($type, $v, $callback, $trait = null) {
        if($class = ClassUtils::getClass($type)) {
            $has = collect($class::_options($v))->contains(TraitUtils::getType($trait));
            if(($trait === null && !$has) || $has) {
                return $callback($class, $v);
            }
            return ResponseUtils::returnRequest('Class `"' . $class . '" dont extends trait "' . $trait . '"', 403);
        }
        return ResponseUtils::returnRequest('Class "' . $class . '" not found in supported for api', 403);
    }

}
