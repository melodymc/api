<?php
namespace App\Utils;

use App\Entities\Abstractions\Api\BasicApi;
use App\Entities\Abstractions\Api\OptionTrait;
use HaydenPierce\ClassFinder\ClassFinder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ClassUtils
{

    public static function getTypeByClass($class, $mask = '/((?i)(\w+)+$)/m') {
        preg_match($mask, $class, $matches, PREG_OFFSET_CAPTURE, 0);
        if(array_key_exists(1, $matches)) {
            if(array_key_exists(0, $matches[1])) {
                return $matches[1][0];
            }
        }
        return null;
    }

    /**
     * @param $type
     * @param bool $clearCache
     * @return string|null
     */
    public static function getClass($type, $clearCache = false)
    {
        foreach(self::getEntityClasses($clearCache) as $class) {
            preg_match_all('/((?i)(' . $type . ')+$)/m', $class, $matches, PREG_SET_ORDER, 0);
            if($matches !== []) {
                if(is_subclass_of($class, BasicApi::class)) {
                    return $class;
                }
            }
        }
        return null;
    }

    /**
     * @param bool $clearCache
     * @return array
     */
    public static function getEntityClasses($clearCache = false)
    {
        return self::getClasses('entityClasses', 'App\Entities', $clearCache);
    }

    public static function getOptionsEnum($clearCache = false) {
        $returned = [];
        foreach (self::getClasses('options', 'App\Entities\Abstractions\Api', $clearCache, 1) as $class) {
            try{
                if(TraitUtils::useTrait($class, OptionTrait::class) && ($type = self::getTypeByClass($class, '((?i)(\w+)+(Trait)$)'))) {
                    $returned[$class] = Str::upper($type);
                }
            } catch (\Exception $e) {}
        }
        return $returned;
    }

    public static function getClasses($key, $namespace, $clearCache = false, $type = 2) {
        return self::storeInCacheForeverOrRewrite($key, function () use ($type, $namespace) {
            return array_values(ClassFinder::getClassesInNamespace($namespace, $type));
        }, $clearCache);
    }

    /**
     * @param $key
     * @param \Closure $callback
     * @param bool $clearCache
     * @return mixed
     */
    public static function storeInCacheForeverOrRewrite($key, $callback, $clearCache = false) {
        if($clearCache) {
            Cache::forget($key);
        }
        return Cache::rememberForever($key, $callback);
    }

}