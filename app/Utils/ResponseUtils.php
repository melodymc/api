<?php

namespace App\Utils;


use Illuminate\Support\Facades\Log;

class ResponseUtils
{

    /**
     * @param $message
     * @param int $s
     * @return string
     */
    public static function returnErrorMessageRequest($message, $s = 200) {
        if($s !== 200) {
            Log::error('[ResponseError]: "' . $s .'" error code - ' . $message);
        }
        return $message;
    }

    /**
     * @param $message
     * @param $s
     * @return \Illuminate\Http\JsonResponse
     */
    public static function returnRequest($message, $s) {
        return response()->make(self::returnErrorMessageRequest($message, $s), $s);
    }

}