<?php
namespace App\Utils;

class TraitUtils
{

    /**
     * @param $class
     * @param $findTrait
     * @return string|null
     */
    public static function useTrait($class, $findTrait)
    {
        $traits = class_uses($class);
        foreach ($traits as $trait) {
            if($trait == $findTrait) {
                return true;
            }
        }
        return false;
    }

    public static function getType($trait, $mode = false)
    {
        foreach (ClassUtils::getOptionsEnum($mode) as $key => $type) {
            if($key == $trait) {
                return $type;
            }
        }
        return null;
    }
}