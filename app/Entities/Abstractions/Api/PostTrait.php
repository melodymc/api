<?php

namespace App\Entities\Abstractions\Api;

trait PostTrait
{

    use OptionTrait;

    /**
     * @param $version
     * @param array $data
     * @return string|null
     * @throws \Exception
     */
    public static function _postDo($version, $data) {
        $class = static::class;
        if(is_subclass_of($class, BasicApi::class)) {
            $obj = new $class($data);
            $obj->save();
            return $obj;
        }
        return null;
    }

}