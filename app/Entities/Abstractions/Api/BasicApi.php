<?php

namespace App\Entities\Abstractions\Api;

use App\Utils\TraitUtils;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class BasicApi extends Model
{
    /**
     * @return array|null
     */
    public static function _options($version) {
        return static::optionsDo($version);
    }

    public static function optionsDo($version) {
        $class = static::class;
        $traits = [];
        if(is_subclass_of($class, BasicApi::class)) {
            foreach (class_uses($class) as $trait) {
                if(TraitUtils::useTrait($trait, OptionTrait::class)) {
                    if($type = TraitUtils::getType($trait)) {
                        $traits[] = $type;
                    }
                }
            }
        }
        return $traits;
    }

    public static function getCurrent($version, $id, $data = null, $mode = null) {
//        if($data === null && ($mode !== null || !$mode || $mode)) {
//            throw new \Exception('lpz send data');
//        }
        if($data === null) {
            $data = ['*'];
        }
        if($mode === null) {
            $mode = true;
        }
        $class = static::class;
        /** @var Builder $obj */
        $obj = $class::where(['id' => $id]);
        $fields = null;
        if($mode) {
            $fields = $data;
        } elseif($data !== ['*']) {
            $fields = collect($obj->first()->getAttributes())->keys()->filter(function ($key) use ($data) {
                return !in_array($key, $data);
            })->toArray();
        }
        $obj = $obj->first($fields);
        $arr = $obj->toArray();
        /** @var GetTrait $obj */
        if($meta = $obj->getCurrentDo($version)) {
            foreach ($meta as $key => $value) {
                if(array_key_exists($key, $arr) && $value === null) {
                    unset($arr[$key]);
                } else {
                    $arr[$key] = $value;
                }
            }
        }
        return $arr;
    }

    /**
     * @param int $version
     * @param array $data
     * @param bool $mode
     * @return mixed
     */
    public static function getAll($version, $data = null, $mode = null) {
        try{
            return static::getAllDo($version, $data, $mode);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $version
     * @param array $data
     * @return mixed
     */
    public function _patch($version, $data) {
        try{
            return $this->_patchDo($version, $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $version
     * @param array $data
     * @return mixed
     */
    public function _put($version, $data) {
        try{
            return $this->_putDo($version, $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $version
     * @param array $data
     * @return mixed
     */
    public static function _post($version, $data) {
        try{
            return static::_postDo($version, $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $version
     * @return mixed
     */
    public function _delete($version) {
        try{
            return $this->_deleteDo($version);
        } catch (\Exception $e) {
            return null;
        }
    }

}