<?php

namespace App\Entities\Abstractions\Api;

trait PatchTrait
{

    use OptionTrait;

    /**
     * @param $version
     * @param array $data
     * @return string|null
     */
    public function _patchDo($version, $data) {
        if($this instanceof BasicApi) {
            $this->fill($data);
            $this->save();
            return $this;
        }
        return null;
    }

}