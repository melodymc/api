<?php

namespace App\Entities\Abstractions\Api;

use App\Utils\ResponseUtils;

trait GetTrait
{

    use OptionTrait;

    public function getCurrentDo($version) {
        return null;
    }

    public static function getAllDo($version, $data, $mode) {
        if(is_subclass_of(static::class, BasicApi::class)) {
            if($all = self::all()) {
                $return = [];
                foreach ($all as $api) {
                    try{
                        $return[] = $api->getCurrent($version, $api->id, $data, $mode);
                    } catch (\Exception $e) {
                        return ResponseUtils::returnRequest($e->getMessage(), 500);
                    }
                }
                return $return;
            }
            return ResponseUtils::returnErrorMessageRequest('*Mister Bin Meme Magic* if you see this error php is broken', 500);
        }
        return ResponseUtils::returnErrorMessageRequest('not supported type', 500);
    }

}