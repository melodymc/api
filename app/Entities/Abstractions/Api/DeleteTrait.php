<?php

namespace App\Entities\Abstractions\Api;

trait DeleteTrait
{

    use OptionTrait;

    /**
     * @param $version
     * @return boolean|null
     */
    public function _deleteDo($version) {
        if($this instanceof BasicApi) {
            try{
                return $this->delete();
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

}