<?php

namespace App\Entities\Abstractions\Api;

trait PutTrait
{

    use OptionTrait;

    /**
     * @param $version
     * @param array $data
     * @return string|null
     * @throws \Exception
     */
    public function _putDo($version, $data) {
        if($this instanceof BasicApi) {
            if(array_key_exists('id', $data)) {
                unset($data['id']);
            }
            $this->fill($data);
            $this->save();
            return true;
        }
        return false;
    }

}