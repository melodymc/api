<?php

namespace App\Entities;

use App\Entities\Abstractions\Api\BasicApi;
use App\Entities\Abstractions\Api\DeleteTrait;
use App\Entities\Abstractions\Api\GetTrait;
use App\Entities\Abstractions\Api\PostTrait;
use App\Entities\Abstractions\Api\PatchTrait;
use App\Entities\Abstractions\Api\PutTrait;
use App\Entities\Economic\Currency\Currency;
use App\Entities\Economic\Wallet;
use App\Entities\Pets\AccountPet;
use App\Entities\Pets\Pet;

class Account extends BasicApi
{
    protected $fillable = ['uuid'];

    use GetTrait, PatchTrait, PostTrait, PutTrait, DeleteTrait;

    public function getBalances() {
        $data = [];
        foreach ($this->wallets()->get() as $wallet) {
            $currencyId = $wallet->currency_id;
            $data[$currencyId !== null ? $currencyId : 0] = self::getBalance($currencyId);
        }
        return $data;
    }

    public function getBalance($currencyId = null) {
        if($wallet = $this->getWallet($currencyId)) {
            return $wallet->getBalance();
        }
        return null;
    }

    public function getCurrentDo($version)
    {
        $data = [];
        $balances = $this->getBalances();
        $data['balances'] = $balances;
        switch ($version) {
            case 2:
                unset($data['balances']);
                $data['balances_v2'] = $balances;
                break;
        }
        return $data;
    }

    /**
     * @param $value
     * @param null $currencyId
     * @param null $note
     * @return bool
     * @throws \App\Exceptions\WalletOperationException
     */
    public function changeBalance($value, $currencyId = null, $note = null) {
        if($wallet = $this->getWallet($currencyId)) {
            return $wallet->changeBalance($value, $note) > 0;
        }
        return false;
    }

    /**
     * @param null $currencyId
     * @return Wallet
     */
    public function getWallet($currencyId = null) {
        return $this->wallets()->where(['currency_id' => $currencyId])->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }

    public function pets()
    {
        return $this->belongsToMany(Pet::class,AccountPet::class);
    }

}