<?php

namespace App\Entities\Economic\Currency;

use App\Entities\Abstractions\Api\PostTrait;
use App\Entities\Abstractions\Api\BasicApi;
use App\Entities\Abstractions\Api\GetTrait;
use App\Entities\Economic\Operation;
use App\Entities\Economic\Wallet;
use App\Entities\Pets\Pet;

/**
 * @property mixed exchange_currency_id
 */
class Currency extends BasicApi
{

    use GetTrait {
        getAllDo as public getAllDoFromTrait;
    }
//    use PostTrait;

//    protected $fillable = ['code'];
//
//    public static function optionsDo($version)
//    {
//        $data = parent::optionsDo($version); // получаем все доступные по динамической выборке трейтов
//        if($version == 1) {
//            if (($key = array_search("POST", $data)) !== false) {
//                unset($data[$key]);
//            }
//        }
//        return $data;
//    }

    public static function getDefaultCurrency()
    {
        return ['id' => 0, 'changeType' => 0, 'isDefault' => true, 'saleExchangeRate' => 1, 'created_at' => null, 'updated_at' => null];
    }

    public function getCurrentDo($version)
    {
        $arr = ['isDefault' => false];
        if(!$this->exchange_currency_id) {
            $arr['exchange_currency_id'] = 0;
        }
        return $arr;
    }

    public static function getAllDo($version, $data, $mode) {
        $arr = [self::getDefaultCurrency()];
        foreach (self::getAllDoFromTrait($version, $data, $mode) as $item) {
            $arr[] = $item;
        }
        return $arr;
    }

    public function wallets()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function operations()
    {
        return $this->belongsTo(Operation::class);
    }

}