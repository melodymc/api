<?php

namespace App\Entities\Economic\Currency;

use Illuminate\Database\Eloquent\Model;

trait CurrencyGetTrait
{

    public function currency()
    {
        if($this instanceof Model) {
            return $this->belongsTo(Currency::class);
        }
        return null;
    }

}