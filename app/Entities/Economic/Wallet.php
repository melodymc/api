<?php

namespace App\Entities\Economic;

use App\Entities\Account;
use App\Entities\Economic\Currency\CurrencyGetTrait;
use App\Exceptions\WalletOperationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * @property int currency_id
 * @property int id
 */
class Wallet extends Model
{
    use CurrencyGetTrait;

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function hasBalance($value) {
        return $this->getBalance() - $value > 0;
    }

    /**
     * получить баланс в выбранной валюте кашелька
     * @return double
     */
    public function getBalance() {
        return (double) $this->operations()->sum('value');
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }

    /**
     * @param $value
     * @param null $note
     * @return double
     * @throws WalletOperationException
     */
    public function changeBalance($value, $note = null)
    {
        if(is_numeric($value)) {
            if($value < 0 && ($this->getBalance() + $value) < 0) {
                throw new WalletOperationException($this->currency_id, $this->id, 2, 'not enough money for this pay');
            }
            $operation = new Operation();
            $operation->setAttribute('value', (double) $value);
            $operation->setAttribute('note', $note);
            $operation->wallet()->associate($this);
            $operation->save();
            return $this->getBalance();
        }
        throw new WalletOperationException($this->currency_id, $this->id, 1, 'for pay need numeric value. this value: ' . $value);
    }

}