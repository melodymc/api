<?php

namespace App\Entities\Economic;

use App\Entities\Economic\Currency\CurrencyGetTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean type
 */
class Operation extends Model
{

    use CurrencyGetTrait;

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

}