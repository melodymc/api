<?php

namespace App\Entities\Pets;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int exp
 */
class AccountPet extends Model
{

    protected $casts = ['exp' => 'integer'];
}