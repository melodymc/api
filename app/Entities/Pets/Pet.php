<?php

namespace App\Entities\Pets;

use App\Entities\Abstractions\Api\BasicApi;
use App\Entities\Abstractions\Api\GetTrait;
use App\Entities\Abstractions\Api\PatchTrait;
use App\Entities\Account;
use App\Entities\Economic\Currency\CurrencyGetTrait;
use App\Exceptions\PetBuyException;
use App\Exceptions\WalletOperationException;
use Exception;
use Illuminate\Support\Facades\Validator;

/**
 * @property int currency_id
 * @property int id
 */
class Pet extends BasicApi
{
    use GetTrait;
    use CurrencyGetTrait;
    use PatchTrait;

    public function getCurrentDo($version)
    {
        return ['prices' => $this->getPricesForApi()];
    }

    public function _patchDo($version, $data) {
        $validator = Validator::make($data, ['account_id' => 'required|exists:accounts,id', 'currency_id' => 'required|exists:currencies,id']);
        try{
            if($this instanceof BasicApi) { // TODO добавить верификацию покупки авторизованным юзером
                if(!$validator->fails()) {
                    try{
                        $msg = $this->buy(Account::find($data['account_id']), $data['currency_id']);
                    } catch (PetBuyException $e) {
                        $msg = $e->getMessage();
                    }
                } else {
                    $msg = $validator->failed();
                }
                return $this->toArray() + ['actions' => ['buy' => $msg]];
            }
        } catch (Exception $ee) {
            echo $ee->getMessage();
        }
        return null;
    }

    /**
     * @param Account $account
     * @param $currencyId
     * @return string
     * @throws PetBuyException
     */
    public function buy(Account $account, $currencyId) {
        if($cost = $this->prices()->where(['currency_id' => $currencyId])->first()) {
            try{
                if($account->changeBalance(-$cost->cost, $currencyId, 'Buy pet by id ' . $this->id) >= 0) {
                    $account->pets()->save($this);
                    return 'done';
                }
            } catch (WalletOperationException $e) {
                throw new PetBuyException($currencyId, 1, 'wallet error : ' . $e->getMessage() .' - code ' . $e->type);
            }
        }
        throw new PetBuyException($currencyId, 0, 'not found by this currency_id pet price');
    }

    private function getPricesForApi()
    {
        return $this->getPrices()->map(function (PetsPrice $pivot) {
            return $pivot->toArray();
        });
    }

    private function getPrices()
    {
        return $this->prices()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    private function prices()
    {
        return $this->hasMany(PetsPrice::class, 'pet_id')->with('currency');
    }

}