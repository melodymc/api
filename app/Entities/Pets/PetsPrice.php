<?php

namespace App\Entities\Pets;

use App\Entities\Economic\Currency\Currency;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int exp
 */
class PetsPrice extends Model
{

    protected $casts = ['cost' => 'integer'];

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

}