<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class PetBuyException extends Exception
{

    public $currency_id;
    public $type;

    public function __construct($currency_id, $type, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->currency_id = $currency_id;
        $this->type = $type;
        parent::__construct($message, $code, $previous);
    }

    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response();
    }
}