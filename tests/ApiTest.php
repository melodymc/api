<?php

use App\Entities\Abstractions\Api\GetTrait;
use App\Utils\ClassUtils;
use App\Utils\TraitUtils;

class ApiTest extends TestCase
{
    public function testGetAllEntities()
    {
        $bool = true;
        $entities = ClassUtils::getEntityClasses();
        foreach ($entities as $class) {
            if(TraitUtils::useTrait($class, GetTrait::class)) {
                $url = route('_get', ["v" => 1, "type" => ClassUtils::getTypeByClass($class)]);
                $this->get($url);
                if(!$this->response->isOk()) {
                    unset($class);
                }
                dump($this->response->content());
            }
        }
        $this->assertTrue($bool
//            $this->app->version(), $this->response->getContent()
        );
    }

    public function testGetPrices() {

        $this->assertTrue(true);
    }
}